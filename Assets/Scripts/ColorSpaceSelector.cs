﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ColorSpaceSelector : MonoBehaviour
{
    public OVRManager oculusManager;

    public List<string> _colorSpaces;
    TMP_Dropdown _dropdown;
    // Start is called before the first frame update
    void Start()
    {
        //Hook up components
        _dropdown = GetComponent<TMP_Dropdown>();

        //clear dropdown
        _dropdown.ClearOptions();
        
        //Get Available Color Spaces from OVRManager
        string[] enumNames = Enum.GetNames(typeof(OVRManager.ColorSpace));
        foreach (string colorSpace in enumNames)
        {
            _colorSpaces.Add(colorSpace);
        }

        //Add color spaces to Dropdown Menu
        _dropdown.AddOptions(_colorSpaces);
    }

    public void SwitchColorSpace(int value)
    {
        oculusManager.colorGamut = (OVRManager.ColorSpace)value;
    }
}
