﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SourceSelectToggleUI : MonoBehaviour
{
    public InputActionReference toggleSource;
    public GameObject sourceWindow;

    private void Start()
    {
        toggleSource.action.performed += ToggleSourceSelect;
    }

    private void ToggleSourceSelect(InputAction.CallbackContext obj)
    {
        if (sourceWindow.activeInHierarchy)
        {
            sourceWindow.SetActive(false);
        }
        else
        {
            sourceWindow.SetActive(true);
        }
    }

}
