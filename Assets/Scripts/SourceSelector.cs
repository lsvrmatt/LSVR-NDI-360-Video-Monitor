﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Klak.Ndi;
using TMPro;

public class SourceSelector : MonoBehaviour
{
    [SerializeField] TMP_Dropdown _dropdown = null;

    public NdiReceiver _receiver;
    public List<string> _sourceNames;
    //bool _disableCallback;

    // HACK: Assuming that the dropdown has more than
    // three child objects only while it's opened.
    //bool IsOpened => _dropdown.isActiveAnd

    void OnEnable()
    {
        // Do nothing if the menu is opened.
        //if (IsOpened) return;

        _dropdown.ClearOptions();

        // NDI source name retrieval
        _sourceNames = NdiFinder.sourceNames.ToList();

        // Currect selection
        var index = _sourceNames.IndexOf(_receiver.ndiName);

        // Append the current name to the list if it's not found.
        if (index < 0)
        {
            index = _sourceNames.Count;
            _sourceNames.Add(_receiver.ndiName);
        }

        // Disable the callback while updating the menu options.
        //_disableCallback = true;

        // Menu option update

        _dropdown.AddOptions(_sourceNames);
        _dropdown.value = index;
        _dropdown.RefreshShownValue();

        // Resume the callback.
        //_disableCallback = false;
    }

    public void OnChangeValue(int value)
    {
        Debug.Log("Changing Source");
        _receiver.ndiName = _sourceNames[value];

    }
}