﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockSpherePositionToHead : MonoBehaviour
{
    public Transform head;

    private void Update()
    {
        transform.position = head.position;
    }
}
