﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateSphereShader : MonoBehaviour
{
    Renderer _renderer;
    void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
    }

    public void SetSphere3DLayout(bool enabled)
    {
        if (enabled)
            _renderer.material.SetFloat("_Layout", 2.0f);
        else if (!enabled)
            _renderer.material.SetFloat("_Layout", 0.0f);
    }
}
