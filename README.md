LSVR NDI 360 Video Monitor
Release v0.1.0

HOW TO USE
To use, you must have an NDI source. The NDI Tools are available for free at https://www.ndi.tv/tools/

When installing, be sure to install all of the options, which includes an Adobe CC plugin allowing you to send the output of Premiere Pro to NDI.

CONTROLS
To bring up the menu, press A on your Oculus Touch Controller. Here you can select your NDI source and your Color Space. There is also a toggle to enable viewing of top/bottom stereoscopic videos. Press A to hide the menu again.

OTHER
This is alpha release software that has not been bug tested. It is use at your own discretion, but happy to help answer any questions and listen to feedback/feature requests. Email: matt@lightsailvr.com
